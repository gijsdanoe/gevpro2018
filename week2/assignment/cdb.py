#!/usr/bin/python3
# function name: cdb.py
# function: this function returns only the adjectives from a XML file.
# name: Gijs Danoe
# date: 16-2-2018

import xml.etree.ElementTree as ET
from collections import OrderedDict
import sys

def get_adjectives():
	tree = ET.parse(sys.argv[1])
	words = tree.getroot()
	adj_list = [cid.attrib['form'] for cid in words if cid.attrib['pos'] == 'ADJ']
	uniq_adj_list = OrderedDict.fromkeys(adj_list)
	return "\n".join(uniq_adj_list)
