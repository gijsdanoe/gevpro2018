#!/usr/bin/python3
# function name: wordsearch.py
# function: this function solves word search puzzles and displays the words found.
# name: Gijs Danoe
# date: 18-2-2018

from collections import defaultdict
import json

def solve(file):
    with open('words.json','r',encoding='utf-8') as f:
        data = json.load(f)['words']
    solutions = set([item.upper() for item in data])
    puzzle = open(file,'r').read().split('\n')[:-1]
    found_words = []
    left_to_right = {}
    top_to_bottom = {}
    directions = [left_to_right, top_to_bottom]
    for line in puzzle:
        left_to_right[puzzle.index(line)] = line
    for i in range(len(puzzle)):
        top_to_bottom[i] = [(line[i]) for line in puzzle]
        top_to_bottom[i] = (''.join(top_to_bottom))
        for direction in directions:
            for line in direction:
                for word in solutions:
                    if word in direction[line]:
                        found_words.append(word)
    return found_words
