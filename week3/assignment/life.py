#!/usr/bin/python3
# program name: life.py
# program function: this function simulates a game of life out of a text file input.
# name: Gijs Danoe
# date: 20-2-2018

from collections import Counter
import sys
from graphics import *


def read_pattern(file):
	coords = []
	with open(file, 'r') as pattern:
		linecount = 0
		for line in pattern:
			linecount += 1
			charcount = 0
			for ch in line:
				charcount += 1
				if ch == "*":
					coords.append((charcount, linecount))
	return coords

def neighbors(coords):
	c = Counter()
	for ch in coords:
		c[(ch[0] + 1, ch[1] + 1)] += 1
		c[(ch[0] + 1, ch[1] - 1)] += 1
		c[(ch[0] - 1, ch[1] + 1)] += 1
		c[(ch[0] - 1, ch[1] - 1)] += 1
		c[(ch[0] + 0, ch[1] + 1)] += 1
		c[(ch[0] + 0, ch[1] - 1)] += 1
		c[(ch[0] + 1, ch[1] + 0)] += 1
		c[(ch[0] - 1, ch[1] + 0)] += 1
	return c

def next_generation(coords, c):
	for key in list(c.keys()):
		if c[key] < 2:
			del c[key]
		elif c[key] > 3:
			del c[key]
	return list(c.keys())

def main():
	win = GraphWin("Game of Life", 500, 500)
	win.setCoords(-35, -35, 35, 35)
	file = sys.argv[1]
	coords = read_pattern(file)
	while True:
		for coord in coords:
			x = coord[0]
			y = coord[1]
			cell = Rectangle(Point(x-1, y), Point(x, y+1))
			cell.setFill("white")
			cell.draw(win)
		win.delete("all")
		c = neighbors(coords)
		coords = next_generation(coords, c)
	win.getMouse()
main()
