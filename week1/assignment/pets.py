class Pet:

        def __init__(self, name):
                self.name = name

        def greeting(self):
                return 'I am {0}!'.format(self.name)


class Dog(Pet):

        def sound(self):
                return 'Woof'

        def greeting(self):
                return '{0}, I am {1}!'.format(self.sound(), self.name)


class Cat(Pet):
        def sound(self):
                return "Meow"

cat = Cat('Lord Purr')
dog = Dog('Peaches')
print(cat.greeting())
print(dog.greeting())
