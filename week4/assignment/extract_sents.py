#!/usr/bin/python3
# program name: extract_sents.py
# function: this program prints all tokens of a text.
# name: Gijs Danoe
# date: 28-2-2018

import sys
import re
import gzip


def tokenizer(file):
    linelist = []
    p = re.compile("[^ ].*?[A-Z]*[a-z][áéúóí]*[.?!][.']*")
    with gzip.open(file, 'rt') as text:
        for line in text:
            line = line.strip()
            found_strings = p.findall(line)
            linelist.append(found_strings)
            ll = [item for sublist in linelist for item in sublist]
    final_linelist = []
    for line in ll:
        s = re.sub(
                    '(?<=[^ ])(?=[.,:;!?()"\'])|(?<=[.,!?:;()"\'])'
                    '(?=[^ ])', r' ', line)
        final_linelist.append(s)
    return final_linelist


def main():
    file = str(sys.argv[1])
    tokenized_text = '\n'.join(tokenizer(file))
    print(tokenized_text.strip(' '))

if __name__ == "__main__":
    main()
